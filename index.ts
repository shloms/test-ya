import { resolve } from 'path';
import wkhtmltopdf from 'wkhtmltopdf';

wkhtmltopdf.command = resolve(__dirname, `wkhtmltopdf.exe`);

wkhtmltopdf('https://apple.com/', { 
    output: 'apple.pdf',
});